﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>stringBling</title>
</head>
<body>
	<form id="form1" runat="server">
	    <h1>String Bling</h1>
        <label>Let's check to see if a word is a palindrome: </label>
        <asp:TextBox runat="server" ID="userWordInput"></asp:TextBox><br>
            <asp:RequiredFieldValidator ID="userWordInputValidator" ControlToValidate="userWordInput" runat="server" ErrorMessage="Please submit a word!" ForeColor="red"></asp:RequiredFieldValidator><br>
            <asp:RegularExpressionValidator ID="userWordInputRegEx" ControlToValidate="userWordInput" runat="server" ErrorMessage="Not a string. Sorry!" ForeColor="red" ValidationExpression="^[a-zA-Z0-9_ ]*$"></asp:RegularExpressionValidator>
            <br>
            <asp:Button runat="server" ID="userSubmitWord" Text="Submit"></asp:Button>
            <br>
            <label>We will let you know if your word(s) results in a palidrome</label>
            <div id="wordResults" runat="server"> 
            </div>
	</form>
    <script src="stringJSFile.js" language="javascript" type="text/javascript"></script>
</body>
</html>
