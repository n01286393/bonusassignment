﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>prime</title>
</head>
<body>
	<form id="form1" runat="server">
	    <h1>Prime Number</h1>
        <label> Please insert a number greater than 1: </label> 
            <asp:TextBox runat="server" ID="userPrimeNumInput"></asp:TextBox><br>
            <asp:RequiredFieldValidator runat="server" ID="userPrimeNumInputValidator" ControlToValidate="userPrimeNumInput" ErrorMessage="Please enter a number!" ForeColor="Red"></asp:RequiredFieldValidator>
            <br>
            <asp:Button runat="server" ID="userSubmitPrime" Text="Submit"></asp:Button>
            <br>
            <label>We will let you know if your number is a prime</label>
            <div id="primeResults" runat="server"> 
            </div>
	</form>
    <script src="primeJSFile.js" language="javascript" type="text/javascript"></script>
</body>
</html>
