﻿document.getElementById("form1").addEventListener("submit", function(event){

    event.preventDefault();

    //variable for my string bling algorithm getting the value of the user input
    const str = document.getElementById("userWordInput").value;

    //
    const result = document.getElementById('wordResults');
    result.innerHTML = stringBling(str);
});

//function that checks in user inputed a string
function stringBling(str) {
    if(regexDigits.test(str) === true || str === '' || regexSymbols.test(str) === true) {
        return 'Only words. No cheating';
    } else{
    str = str.toLowerCase().replace(" ", "");
    if((str == str.split('').reverse().join('')) === true) return 'Palidrome!';
    else return 'Sorry, not a palidrome.';
    }
}

const regexDigits = /\d/;
const regexSymbols = /[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/;