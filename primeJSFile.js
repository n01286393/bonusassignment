﻿document.getElementById("form1").addEventListener("submit", function(event){
    event.preventDefault();

    var num = parseInt(document.getElementById("userPrimeNumInput").value);

    let result = document.getElementById('primeResults');
    result.innerHTML = primeNumber(num);
});

//this function determines if a number if prime
function primeNumber(num) {
    //this is for my result message
    let res = '';
    if(num < 2) alert("Please insert a number greater than 1.");
    for (let i = 2; i < num; i++) {
        if(num%i==0)
            return res = 'Not a Prime!';
    }
    return res = 'Prime!';
}