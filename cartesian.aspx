﻿<%@ Page Language="C#" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>cartesian</title>
     
</head>
<body>
	   <form id="form1" runat="server">
            <h1>Cartesian</h1>
            <label> Please enter an x coordinate:</label> 
            <asp:TextBox runat="server" ID="userXInput"></asp:TextBox><br>
            <asp:RequiredFieldValidator runat="server" ID="userXInputValidator" ControlToValidate="userXInput" ErrorMessage="Please enter an x coordinate" ForeColor="Red"></asp:RequiredFieldValidator>
            <br>
            <label> Please enter a y coordinate:</label>
            <asp:TextBox runat="server" ID="userYInput"></asp:TextBox><br>
            <asp:RequiredFieldValidator runat="server" ID="userYInputValidator" ControlToValidate="userYInput" ErrorMessage="Please enter a y coordinate" ForeColor="Red"></asp:RequiredFieldValidator>
            <br>
            <asp:Button runat="server" ID="userSubmit" Text="Submit"></asp:Button>
            <br>
            <label>Based on your coordinates, the coordinate will fall on:</label>
            <div id="cartesianResults" runat="server"> 
            </div>
    </form>
    <script src="JSFile.js" language="javascript" type="text/javascript"></script>
</body>
</html>
