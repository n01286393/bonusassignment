﻿

document.getElementById("form1").addEventListener("submit", function(event){
    event.preventDefault();

    //variables for cartersian algorithm 
    var x = parseInt(document.getElementById("userXInput").value);
    var y = parseInt(document.getElementById("userYInput").value);

    let results = document.getElementById('cartesianResults');
    results.innerHTML = cartesianMapFunction(x, y);
});
    
//this function checks x and y coordinates to see which quadrant it falls into
//this function is really general so I can reuse it for other applications
function cartesianMapFunction(xaxis, yaxis) {
    let res = "";
    if((xaxis > 1) && (yaxis > 1)) res = 'Quandrant I';
    if((xaxis < 1) && (yaxis > 1)) res = 'Quandrant II';
    if((xaxis < 1) && (yaxis < 1)) res = 'Quandrant III';
    if((xaxis > 1) && (yaxis < 1)) res = 'Quandrant IV';
    return res;
}



